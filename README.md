# love2d-matches-man-run
A matches man running game made with [LÖVE](https://love2d.org/).

# Screenshot
![screenshot](https://gitlab.com/okcy1016/love2d-matches-man-run/raw/master/screenshot.gif)
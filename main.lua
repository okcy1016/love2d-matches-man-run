function love.load()
   love.graphics.setColor(0, 0, 0)
   love.graphics.setBackgroundColor(255, 255, 255)
   love.graphics.setFont(love.graphics.newFont("assets/dengxian.ttf", 14))
   gameOver = false
   gameScore = 0
   frameCount = 0

   -- initialize matches man
   matches_man_img_0 = love.graphics.newImage("assets/matches_man_0.png")
   matches_man_img_1 = love.graphics.newImage("assets/matches_man_2.png")
   matches_man_img = matches_man_img_0
   manYSpeed = 0
   -- real height to pixels
   gAcceleration = 22 * (matches_man_img:getHeight() / 1.72)
   manInitialYCoordinate = 600 - matches_man_img:getHeight()
   manYCoordinate = manInitialYCoordinate
   manXCoordinate = 800 / 3 * 2 - matches_man_img:getWidth()
   manInitialJumpYSpeed = -1 * (12 * (matches_man_img:getHeight() / 1.72))

   -- initialize obstacles
   obstacle_img_0 = love.graphics.newImage("assets/obstacle_0.png")
   obs0XCoor = math.random(-300, 0)
   obs0YCoor = 600 - obstacle_img_0:getHeight()
   obsXSpeed = 180
   obs1XCoor = math.random(-800, -500)
   obs1YCoor = 600 - obstacle_img_0:getHeight()
   obs2XCoor = math.random(-1300, -1000)
   obs2YCoor = 600 - obstacle_img_0:getHeight()
   obs3XCoor = math.random(-1800, -1500)
   obs3YCoor = 600 - obstacle_img_0:getHeight()
end

function love.update(deltaTime)
   -- detect jump key press
   if love.keyboard.isDown("space") and manYCoordinate == manInitialYCoordinate then
      manYSpeed = manInitialJumpYSpeed
   end
      
   -- man Y axis update
   if manYCoordinate - manInitialYCoordinate <= 0 then
      -- man Y speed update
      manYSpeed = gAcceleration * deltaTime + manYSpeed
      manYCoordinate = manYCoordinate + manYSpeed * deltaTime
      -- check if the man reaches ground
      if manYCoordinate - manInitialYCoordinate > 0 then
	 manYCoordinate = manInitialYCoordinate
	 manYSpeed = 0
      end
   else
      manYSpeed = 0
      manYCoordinate = manInitialYCoordinate
   end

   -- obstacle coordinate update
   if obs0XCoor >= 800 then
      obs0XCoor = math.random(-300, 0)
   end
   if obs1XCoor >= 800 then
      obs1XCoor = math.random(-400, -100)
   end
   if obs2XCoor >= 800 then
      obs2XCoor = math.random(-500, -200)
   end
   if obs3XCoor >= 800 then
      obs3XCoor = math.random(-600, -300)
   end
   
   obs0XCoor = obs0XCoor + obsXSpeed * deltaTime
   obs1XCoor = obs1XCoor + obsXSpeed * deltaTime
   obs2XCoor = obs2XCoor + obsXSpeed * deltaTime
   obs3XCoor = obs3XCoor + obsXSpeed * deltaTime

   -- detect collision
   if (manXCoordinate <= (obs0XCoor + obstacle_img_0:getWidth()) and (manXCoordinate + matches_man_img:getWidth()) >= obs0XCoor) and ((manYCoordinate + matches_man_img:getHeight()) >= obs0YCoor) then
      gameOver = true
   elseif (manXCoordinate <= (obs1XCoor + obstacle_img_0:getWidth()) and (manXCoordinate + matches_man_img:getWidth()) >= obs1XCoor) and ((manYCoordinate + matches_man_img:getHeight()) >= obs1YCoor) then
      gameOver = true
   elseif (manXCoordinate <= (obs2XCoor + obstacle_img_0:getWidth()) and (manXCoordinate + matches_man_img:getWidth()) >= obs2XCoor) and ((manYCoordinate + matches_man_img:getHeight()) >= obs2YCoor) then
      gameOver = true
   elseif (manXCoordinate <= (obs3XCoor + obstacle_img_0:getWidth()) and (manXCoordinate + matches_man_img:getWidth()) >= obs3XCoor) and ((manYCoordinate + matches_man_img:getHeight()) >= obs3YCoor) then
      gameOver = true
   end

   -- calculate score
   if (manXCoordinate <= (obs0XCoor + obstacle_img_0:getWidth()) and (manXCoordinate + matches_man_img:getWidth()) >= obs0XCoor) and ((manYCoordinate + matches_man_img:getHeight()) < obs0YCoor) then
      gameScore = gameScore + 1
   elseif (manXCoordinate <= (obs1XCoor + obstacle_img_0:getWidth()) and (manXCoordinate + matches_man_img:getWidth()) >= obs1XCoor) and ((manYCoordinate + matches_man_img:getHeight()) < obs1YCoor) then
      gameScore = gameScore + 1
   elseif (manXCoordinate <= (obs2XCoor + obstacle_img_0:getWidth()) and (manXCoordinate + matches_man_img:getWidth()) >= obs2XCoor) and ((manYCoordinate + matches_man_img:getHeight()) < obs2YCoor) then
      gameScore = gameScore + 1
   elseif (manXCoordinate <= (obs3XCoor + obstacle_img_0:getWidth()) and (manXCoordinate + matches_man_img:getWidth()) >= obs3XCoor) and ((manYCoordinate + matches_man_img:getHeight()) < obs3YCoor) then
      gameScore = gameScore + 1
   end

   ---- matches man animation
   -- matches man in air
   if manYCoordinate < manInitialYCoordinate then
      matches_man_img = matches_man_img_1
   else
      frameCount = frameCount + 1
      if frameCount == 15 and manYCoordinate == manInitialYCoordinate then
	 frameCount = 0
	 -- change image
	 if matches_man_img == matches_man_img_0 then
	    matches_man_img = matches_man_img_1
	 else
	    matches_man_img = matches_man_img_0
	 end
      end
   end
end

function love.draw()
   if not gameOver then
      love.graphics.print("得分: " .. gameScore)
      love.graphics.draw(matches_man_img, manXCoordinate, manYCoordinate)
      love.graphics.draw(obstacle_img_0, obs0XCoor, obs0YCoor)
      love.graphics.draw(obstacle_img_0, obs1XCoor, obs1YCoor)
      love.graphics.draw(obstacle_img_0, obs2XCoor, obs2YCoor)
      love.graphics.draw(obstacle_img_0, obs3XCoor, obs3YCoor)
   else      
      love.graphics.print("游戏结束。得分: " .. gameScore .. ".", 100, 100)
   end
end
